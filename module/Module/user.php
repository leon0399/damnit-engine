<?php

/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 17.11.2015
 * Time: 21:41
 */

namespace Module;

use DamnIT\Asset\Manager;
use DamnIT\Store\Breadcrumb;
use DamnIT\Store\Store;
use DamnIT\Template\Layout;
use DamnIT\Template\Single;
use DamnIT\Util\Util;

class user {
	public $template;
	public $cssFiles = [ ];
	public $jsFiles  = [ ];

	public function __construct() {
		$this->smarty = Layout::getTemplate();
	}

	public function registerPage() {
		Store::storeSet('sub-title', 'Регистрация');
		Breadcrumb::add('Регистрация', '/user/register');
		Manager::addDefer('https://www.google.com/recaptcha/api.js');

		$this->template  = 'user/register/register';
		$this->jsFiles[] = 'register';

		$this->smarty->assign('step', 1);
		$this->smarty->assign('username_regex', Util::get_opt('username-regex'));
		$this->smarty->assign('password_regex', Util::get_opt('password-regex'));
		$this->smarty->assign('email_regex', Util::get_opt('email-regex'));

		$publickey = Util::get_opt('recaptcha-key-public');
		$this->smarty->assign('recaptcha', '<div class="g-recaptcha col-xs-6 col-md-6" data-size="normal"
		data-sitekey="' . $publickey . '"></div>');
	}
}