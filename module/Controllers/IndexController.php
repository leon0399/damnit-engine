<?php

namespace Controllers;

use DamnIT\Store\Store;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 24.11.2015
 * Time: 1:20
 */
class IndexController implements \DamnIT\Route\Controller\IBaseController {
	public function registerRoutes(\DamnIT\Route\Route $router) {
		$router->get('/', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
			Store::storeSet('sub-title', 'Главная');

			return $response->write(\DamnIT\Template\Layout::load('home'));
		});
		$router->group('/user', function () use ($router) {
			$router->get('register', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
				$module = new \Module\user();
				$module->registerPage();

				return $response->write(\DamnIT\Template\Layout::load($module->template, $module->cssFiles,
				                                                      $module->jsFiles));
			}
			);
		});
	}
}