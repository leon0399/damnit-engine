<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 28.11.2015
 * Time: 21:47
 */

namespace Controllers;

use DamnIT\Route\Controller\IBaseController;
use DamnIT\Util\Util;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Capsule\Manager as DB;

class CronController implements IBaseController {
	public function registerRoutes(\DamnIT\Route\Route $router) {
		$router->get('/cron/{safety_key}', function (
			ServerRequestInterface $request, ResponseInterface $response,
			$args
		) {
			$response->withHeader('Content-type', 'text/plain');
			if($args['safety_key'] != Util::get_opt('cron-safety-key')) {
				return $response->write('Wrong key passed')->withHeader('Content-type', 'text/plain');
			}

			set_time_limit(59);

			$startTime = microtime();

			for($i = 0; $i < 5; $i++) {
				self::print_pretty('');
			}
			self::print_pretty("START TIME: " . $startTime);

			$jobs = DB::table("cron_job")
				->select([ 'entry_id', 'cron_class', 'cron_method' ])
				->where('active', '=', 1)
				->where ('next_run', '<=', NOW)
				->orderBy('entry_id')->get();

			foreach($jobs as $job) {
				self::print_pretty("JOB START: " . $job->cron_class . '::' . $job->cron_method . '()');
				$next_run = call_user_func_array($job->cron_class . '::' . $job->cron_method, array());
				DB::table("cron_job")->where('entry_id', '=', $job->entry_id)->update(["next_run" => NOW + $next_run]);
				self::print_pretty("JOB STOP");
			}

			$stopTime = microtime();
			self::print_pretty("STOP TIME: " . $stopTime);
			self::print_pretty("EXEC TIME: " . ($stopTime - $startTime));

			return $response->withHeader('Content-type', 'text/plain');
		});
	}

	/**
	 * @param $input string
	 */
	public static function print_pretty($input) {
		$toadd = 120 - strlen($input);
		for($i = 0; $i < $toadd; $i++) {
			$input .= '-';
		}
		print $input . "\n";
	}
}