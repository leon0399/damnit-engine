<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 29.11.2015
 * Time: 0:16
 */

namespace Controllers;

use DamnIT\Route\Controller\IBaseController;
use DamnIT\Util\Util;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class SeoController implements IBaseController {
	public function registerRoutes(\DamnIT\Route\Route $router) {
		$router->get('/ya-tablo-manifest.json',
				function (ServerRequestInterface $request, ResponseInterface $response, $args) {
					$data = array(
							'api_version' => 4,
							'layout'      => array(
									'logo'  => (RURI . "public/Download/img/logo/logo.96.png"),
									'color' => '#333333',
							),
							'feed'        => (RURI . 'ya-tablo-manifest.json'),
					);

					return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES))
					                ->withHeader('Content-type', 'application/json');
				});
		$router->get('/ya-tablo-feed.json',
				function (ServerRequestInterface $request, ResponseInterface $response, $args) {
					$data = array(
							'feed' => array(
									'notifications' => array(
											'%BELL%'    => 0,
											'%EARTH%'   => 5,
											'%FRIEND%'  => 0,
											'%MESSAGE%' => 0,
											'%PHOTO%'   => 0,
											'%CHAT%'    => 0,
									),
									'refresh_time'  => 2,
							),
					);

					return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES))
					                ->withHeader('Content-type', 'application/json');
				});
		$router->get('/web_app_manifest.json',
				function (ServerRequestInterface $request, ResponseInterface $response, $args) {
					$data = array(
							'name'                        => Util::get_opt('site-title'),
							'short_name'                  => Util::get_opt('site-title'),
							'start_url'                   => RURI . 'start?utm_source=web_app_manifest',
							'description'                 => Util::get_opt('site-description'),
							'display'                     => 'standalone',
							'background_color'            => '#333333',
							'lang'                        => 'RU-ru',
							'default_locale'              => 'ru',
							'orientation'                 => 'portrait-primary',
							'prefer_related_applications' => false,
							'developer'                   => array(
									'name' => 'leon0399',
									'url'  => 'http://leon0399.ru/'
							),
							'icons'                       => array(
									'48'  => RURI . 'public/Download/img/logo/logo.touch.48.png',
									'70'  => RURI . 'public/Download/img/logo/logo.touch.70.png',
									'72'  => RURI . 'public/Download/img/logo/logo.touch.72.png',
									'76'  => RURI . 'public/Download/img/logo/logo.touch.76.png',
									'96'  => RURI . 'public/Download/img/logo/logo.touch.96.png',
									'128' => RURI . 'public/Download/img/logo/logo.touch.128.png',
									'120' => RURI . 'public/Download/img/logo/logo.touch.120.png',
									'144' => RURI . 'public/Download/img/logo/logo.touch.144.png',
									'150' => RURI . 'public/Download/img/logo/logo.touch.150.png',
									'152' => RURI . 'public/Download/img/logo/logo.touch.152.png',
									'168' => RURI . 'public/Download/img/logo/logo.touch.168.png',
									'192' => RURI . 'public/Download/img/logo/logo.touch.192.png',
									'200' => RURI . 'public/Download/img/logo/logo.touch.200.png',
									'256' => RURI . 'public/Download/img/logo/logo.touch.256.png',
									'512' => RURI . 'public/Download/img/logo/logo.touch.512.png',
									'1024' => RURI . 'public/Download/img/logo/logo.touch.1024.png',
									'2048' => RURI . 'public/Download/img/logo/logo.touch.2048.png',
							),
							'splash_screens'              => array(
									array(
											'src'   => RURI . 'public/Download/img/splash/splash.320x240.png',
											'sizes' => '320x240',
									),
									array(
											'src'   => RURI . 'public/Download/img/splash/splash.1334x750.png',
											'sizes' => '1334x750',
									),
									array(
											'src'     => RURI . 'public/Download/img/splash/splash.1920x1080.png',
											'sizes'   => '1920x1080',
											'density' => 3
									),
							),
					);

					return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES))
					                ->withHeader('Content-type', 'application/x-web-app-manifest+json');
				});
	}
}