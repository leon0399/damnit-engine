<?php

namespace Controllers;

use DamnIT\Api\UsersApi;
use DamnIT\Route\Controller\IBaseController;
use DamnIT\Util\UUID;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Capsule\Manager as DB;
use Service\Crypt\Crypton;
use Service\NoSpam\NoSpam;

class ApiController implements IBaseController {
	public function __construct() {
	}

	public function __autoload($path) {
		echo $path;
	}

	public function registerRoutes(\DamnIT\Route\Route $router) {
		$router->group('/api', function () use ($router) {
			$router->group('/users', function () use ($router) {
				$router->get(
					'', function (ServerRequestInterface $request, ResponseInterface $response) {
					$api = new UsersApi();
					$api->getAll();

					return $response->write(json_encode($api->getResponse()));
				});

				$router->get('/new', function (ServerRequestInterface $request, ResponseInterface $response) {
					$v5              = UUID::v5(UUID::v4(), openssl_random_pseudo_bytes(16));
					$packed_v5       = pack('h*', str_replace('-', '', $v5));
					$username_pseudo = 'test_' . bin2hex(openssl_random_pseudo_bytes(4));
					$salt            = openssl_random_pseudo_bytes(22);
					$realIp          = NoSpam::getUserRealIP();
					$toIns           = [
						'user_uuid_text'     => $v5,
						'user_uuid_bin'      => $packed_v5,
						'username'           => $username_pseudo,
						'email'              => $username_pseudo . '@paxgame.ru',
						'user_password'      => Crypton::simple(bin2hex
						                                        (openssl_random_pseudo_bytes(32)
						                                        ), $salt),
						'user_password_salt' => $salt,
						'register_date'      => date("Y-m-d H:i:s", NOW),
						'register_ip'        => ip2long($realIp),
						'last_active_date'   => date("Y-m-d H:i:s", NOW),
						'last_active_ip'     => ip2long($realIp),
					];
					DB::table('users')->insert($toIns);

					return $response->write(json_encode($toIns));
				});
				$router->post(
					'', function (ServerRequestInterface $request, ResponseInterface $response) {
					$api = new UsersApi();
					$api->register($_REQUEST);

					return $response->write(json_encode($api->getResponse()));
				});
				$router->get('uuid:{uuid:[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}}',
					function (ServerRequestInterface $request, ResponseInterface $response, $args) {
						$api = new UsersApi($args);
						$api->getByUUID();

						return $response->write(json_encode($api->getResponse()));
					});
				$router->get('{username:' . \DamnIT\Util\Util::get_opt('regex-username')
				             . '+}', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
					$api = new UsersApi($args);
					$api->getByUsername();

					return $response->write(json_encode($api->getResponse()));
				});
				$router->get('{email:' . \DamnIT\Util\Util::get_opt('regex-email')
				             . '+}', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
					$api = new UsersApi($args);
					$api->getByEmail();

					return $response->write(json_encode($api->getResponse()));
				});
			});
			$router->get(
				'/', function (ServerRequestInterface $request, ResponseInterface $response) {
			});
			$router->get(
				'/{invalid}', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
			});/*
			$router->getContainer()['notFoundHandler'] = function ($c) {
				return function ($request, $response) use ($c) {
					return $c['response']
							->withStatus(404)
							->withHeader('Content-Type', 'application/json')
							->write(\DamnIT\Template\Layout::notFound());
				};
			};*/
		})->add(
			function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
				$response = $next($request, $response);
				$response = $response->withHeader('Content-type', 'application/json');

				return $response;
			}
		);
	}
}