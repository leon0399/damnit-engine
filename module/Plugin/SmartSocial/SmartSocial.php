<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 26.01.2016
 * Time: 15:59
 */

namespace Plugin\SmartSocial;

use DamnIT\Hook\HookSystem;
use DamnIT\Plugin\BasePlugin;
use DamnIT\Template\Single;
use DamnIT\Util\Util;

class SmartSocial extends BasePlugin {
	const PLUGIN_NAME = 'SmartSocial';
	const ICONS       = array(
		'vk.com'      => '<i class="fa fa-vk"></i>',
		'twitter.com' => '<i class="fa fa-twitter"></i>',
	);

	public function __construct() {
		HookSystem::addHook("footer_center", function ($template) {
			print "<span style=\"font-size: 25px;\">";
			for($i = 0; $i < sizeof(Util::get_opt("social-links")); $i++) {
				print "<a target=\"_blank\" href=\"" . Util::get_opt("social-links")[$i] . "\">" . self::ICONS[parse_url
					(Util::get_opt("social-links")[$i], PHP_URL_HOST)] .
				      "</a>";
				if($i != sizeof(Util::get_opt("social-links")) - 1) {
					print "&nbsp;";
				}
			}
			print "</span>";
		});

		/*HookSystem::addHook("after_sidebar", function () {
			$toren = new Single(MODULEPATH . 'Plugin' . DIRECTORY_SEPARATOR .
			                    self::PLUGIN_NAME . DIRECTORY_SEPARATOR);
			$toren->display("sidebar_recommend.tpl");
		});*/
	}
}