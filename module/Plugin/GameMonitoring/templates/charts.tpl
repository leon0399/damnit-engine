{literal}
<div id="charts" style="height: 500px; min-width: 500px;"></div>
<script>
    $(function () {
        var seriesOptions = [];
        Highcharts.setOptions({
                                  lang: {
                                      months: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',  'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
                                      weekdays: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
                                      rangeSelectorZoom: 'Масштаб'
                                  },
                                  global: {
                                      useUTC: false
                                  }
                              });
        var createChart = function () {
            console.log(seriesOptions);
            $('#charts').highcharts('StockChart', {
                chart: {
                    type: 'spline',
                    renderTo: 'charts',
                    zoomType: 'x',
                    backgroundColor: '#f9f9f9',
                    events: {
                        load: function () {
                            //clearInterval(updateCharts);
                            //setInterval(updateCharts, 1000*5);

                        }
                    }
                },
                rangeSelector: {
                    buttons: [{
                        type: 'all',
                        text: 'Всё время'
                    }, {
                        type: 'day',
                        count: 31,
                        text: 'Месяц'
                    }, {
                        type: 'day',
                        count: 14,
                        text: '2 недели'
                    }, {
                        type: 'day',
                        count: 7,
                        text: 'Неделя'
                    }, {
                        type: 'day',
                        count: 3,
                        text: '3 дня'
                    }, {
                        type: 'day',
                        count: 1,
                        text: '1 день'
                    }, {
                        type: 'hour',
                        count: 12,
                        text: '12 часов'
                    }, {
                        type: 'hour',
                        count: 3,
                        text: '3 часа'
                    }],
                    inputDateFormat: '%d.%m.%Y',
                    inputEditDateFormat: '%d.%m.%Y',
                    buttonTheme: {
                        width: 60
                    },
                    inputEnabled: false,
                    enabled: true,
                    selected: 10
                },
                title: {
                    text: 'Статистика игроков на сервере'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Количество игроков',
                        style: { "color": "#000000", "font-weight": "normal"}
                    },
                    plotLines: [/*{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }, */{
                        value: 50,
                        color: 'red',
                        dashStyle: 'shortdash',
                        width: 2
                    }],
                    labels: {
                        align: 'left'
                    },
                    allowDecimals: false/*Отключение дробных чисел*/
                },
                xAxis: {

                },
                colors: ['#e2ce38', '#f15c80', '#8085e8', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1'],
                exporting: {
                    enabled: false
                },
                tooltip: {
                    backgroundColor: 'rgba(250, 250, 250, .85)',
                    borderColor: 'rgba(100, 100, 100, .90)',
                    xDateFormat: '%A, %d %B %Y %H:%M',
                    formatter: function(tooltip) {
                        var pointFormat         = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b>';

                        var text = tooltip.tooltipFooterHeaderFormatter(this.points[0]);

                        for(var i = 0, c = this.points.length; i < c; i++) {
                            var point  = this.points[i];
                            point.point.y = Math.ceil(point.point.y);

                            text += point.point.tooltipFormatter(pointFormat)
                                    + '<br/>';
                        }

                        return text;
                    }
                },
                legend: {
                    enabled: true,
                    align: 'center',
                    verticalAlign: 'bottom'
                },

                series: seriesOptions
            });
        };
        var updateCharts = function() {
            $.getJSON("/monitoring/cache/", function (data) {
                //console.log(data);
                $.each(data,  function (key, value) {
                    //console.log(value);
                    seriesOptions[key] = {
                        name: value['title'],
                        type: 'spline',
                        data: (function() {
                            var data = [];
                            for (var i = 0; i < value['data'].length; i++) {
                                data.push([((parseInt(value['data'][i]['time']))*1000), parseInt(value['data'][i]['num_players'])])
                            }
                            return data;
                        })()
                    };
                });
                createChart();
            });
        };
        updateCharts();
        setInterval(updateCharts, 1000*60*5);
    });
</script>
{/literal}