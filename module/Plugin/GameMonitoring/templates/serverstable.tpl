<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="serversTableHeading">
        <a role="button" data-toggle="collapse" href="#serversTable" aria-expanded="true" aria-controls="serversTable">
            Игровые сервера
        </a>
        <a role="button" href="#" class="pull-right" onclick="eModal.ajax({literal}{url: '/monitoring/charts', title: 'Статистика', size: eModal.size.xl, buttons: [{text: 'Закрыть', style: 'primary', close: true}]}{/literal});">
            Статистика
        </a>
    </div>
    <div id="serversTable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="serversTableHeading">
        <table id='game_monitoring' class='table table-bordered table-hover table-condensed game_monitoring' style="background: #fff">
        <thead>
            <tr>
                <td style="width: 16px;"> </td>
                <td style="width: 75px; text-align: center"> Статус: </td>
                <td style="width: 170px; text-align: center"> Адрес: </td>
                <td> Название: </td>
                <td style="width: 70px; text-align: center"> Игроки: </td>
                <td style="width: 70px; text-align: center"> Аптайм: </td>
            </tr>
        </thead>
        <tbody>
            {foreach from=$servers item=server}
                <tr {if $server->cache_state=="offline"} class="danger" {/if} >
                    <td style="text-align: center">
                        <img height="16px" width="16px" src="{$smarty.const.DURI}public/Download/img/gameicon/{$server->type}.png" alt="{$server->type}"/>
                    </td>
                    <td style="text-align: center">{$gamestates["{$server->cache_state}"]}</td>
                    <td style="text-align: center">
                        <input title="Адрес" onmouseover="this.select()" value="{$server->host_alias}" style="background: transparent; border: none; text-align: center" readonly />
                    </td>
                    <td>
                        {$server->title}
                        {if array_key_exists("connect_url", $server->public_data)}
                            <a class="pull-right" href="{$server->public_data["connect_url"]}"><i class="fa fa-plug"></i></a>
                        {/if}
                        {if array_key_exists("map_url", $server->public_data)}
                            <a role="button" onclick="eModal.iframe({literal}{url: '{/literal}{$server->public_data["map_url"]}{literal}', size: eModal.size.xl, title: 'Карта сервера {/literal}{$server->title}{literal}'}{/literal})" class="pull-right" href="#"><i class="fa fa-map"></i></a>
                        {/if}
                    </td>
                    <td style="text-align: center">
                        {if $server->cache_state!="online"}
                            --/--
                        {else}
                            {$server->cache_players}/{$server->cache_maxplayers}
                        {/if}
                    </td>
                    <td style="text-align: center" title="">
                        {if $server->request_times > 0 }
                        {round($server->request_online / $server->request_times * 100, 2)}
                        {else}
                        ?
                        {/if}
                        %
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    </div>
</div>