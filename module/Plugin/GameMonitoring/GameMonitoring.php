<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 16:25
 */

namespace Plugin\GameMonitoring;

use DamnIT\Hook\HookSystem;
use DamnIT\Plugin\BasePlugin;
use DamnIT\Template\Single;
use DamnIT\Util\Util;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Capsule\Manager as DB;
use Smarty_Internal_Template;

class GameMonitoring extends BasePlugin {
	const PLUGIN_NAME = 'GameMonitoring';

	const DB_LIST                   = 'gameservers_list';
	const DB_CACHE                  = 'gameservers_cache';
	const DB_ROW_ID                 = 'id';
	const DB_ROW_TITLE              = 'title';
	const DB_ROW_TYPE               = 'type';
	const DB_ROW_HOST               = 'host';
	const DB_ROW_HOST_ALIAS         = 'host_alias';
	const DB_ROW_CACHE_PLAYERS      = 'cache_players';
	const DB_ROW_CACHE_MAXPLAYERS   = 'cache_maxplayers';
	const DB_ROW_CACHE_STATE        = 'cache_state';
	const DB_ROW_HISTORICAL_PLAYERS = 'historical_players';
	const DB_ROW_HISTORICAL_DATE    = 'historical_date';
	const DB_ROW_REQUEST_TIMES      = 'request_times';
	const DB_ROW_REQUEST_ONLINE     = 'request_online';
	const DB_ROW_PUBLIC_DATA        = 'public_data';
	const DB_ROW_LAST_OFFLINE       = 'last_offline';
	const GAME_STATES = [
		'online'    => '<span class="label label-success">Online</span>',
		'offline'   => '<span class="label label-danger">Offline</span>',
		'techworks' => '<span class="label label-warning">Techwork</span>',
	];

	public function __construct() {
		HookSystem::addHook(
			"home_body_start",
			function ($template) {
				$toren = new Single(MODULEPATH . 'Plugin' . DIRECTORY_SEPARATOR .
				                    self::PLUGIN_NAME . DIRECTORY_SEPARATOR);
				$toren->assign('servers', self::getServersList());
				$toren->assign('gamestates', self::GAME_STATES);
				$toren->display("serverstable.tpl");
			});
	}

	public static function getServersList() {
		$servers    = DB::table("gameservers_list")->select([ self::DB_ROW_ID,
		                                                      self::DB_ROW_TITLE,
		                                                      self::DB_ROW_TYPE,
		                                                      self::DB_ROW_HOST,
		                                                      self::DB_ROW_HOST_ALIAS,
		                                                      self::DB_ROW_CACHE_PLAYERS,
		                                                      self::DB_ROW_CACHE_MAXPLAYERS,
		                                                      self::DB_ROW_CACHE_STATE,
		                                                      self::DB_ROW_HISTORICAL_PLAYERS,
		                                                      self::DB_ROW_HISTORICAL_DATE,
		                                                      self::DB_ROW_REQUEST_TIMES,
		                                                      self::DB_ROW_REQUEST_ONLINE,
		                                                      self::DB_ROW_PUBLIC_DATA,
		                                                      self::DB_ROW_LAST_OFFLINE,
		                                                    ])->where('active', '=', 1)->orderBy("order")->get();
		$newservers = [ ];

		foreach($servers as $server) {
			$server->public_data = Util::valid_JSON($server->public_data);
			$newservers[$server->id] = $server;
		}

		return $newservers;
	}

	public function registerRoutes(\DamnIT\Route\Route $router) {
		$router->get('/monitoring/cache', function (
			ServerRequestInterface $request, ResponseInterface $response, $args
		) {
			$data = [];
			foreach(self::getServersList() as $server) {
				$history = DB::table(self::DB_CACHE)
				             ->select('num_players', 'time')
				             ->where('server_id', '=', $server->id)
				             ->orderBy("time")
				             ->get();
				$data[]  = array(
					'title' => $server->title,
					'data' => $history
				);
			}
			return $response->withHeader("Content-type", "text/json")->write(json_encode($data));
		});
		$router->get('/monitoring/charts', function(ServerRequestInterface $request, ResponseInterface $response, $args) {
			$toren = new Single(MODULEPATH . 'Plugin' . DIRECTORY_SEPARATOR . self::PLUGIN_NAME . DIRECTORY_SEPARATOR);
			$toren->display("charts.tpl");
			//return $response->write();
		});
	}
}