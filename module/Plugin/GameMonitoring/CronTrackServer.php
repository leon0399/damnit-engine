<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 16:31
 */

namespace Plugin\GameMonitoring;

use Controllers\CronController;
use GameQ;
use Illuminate\Database\Capsule\Manager as DB;

class CronTrackServer {
	public static function getCurrentStats() {
		/*$curl = curl_init('https://api.telegram.org/bot137407491:AAFpoA67LOx3O_GV8I4ZsqJVn2g9zcbPLEE/sendMessage
		?chat_id=568908&text=123');
		curl_exec($curl);
		curl_close($curl);*/
		CronController::print_pretty("Using GameQ version: " . GameQ::VERSION);

		DB::table(GameMonitoring::DB_CACHE)->where('time', '<=', NOW - (60 * 60 * 24 * 30))->delete();

		$servers = GameMonitoring::getServersList();
		$gameQ   = GameQ::factory()->setOption('timeout', 3)
		                ->setOption('debug', true)
		                ->setFilter('normalise');
		foreach($servers as $index => $server) {
			!DEBUG or CronController::print_pretty("Server found: " . $server->title . " (ID" . $index . ")");
			$gameQ->addServer(array(
				                  GameQ::SERVER_ID   => $server->id,
				                  GameQ::SERVER_TYPE => $server->type,
				                  GameQ::SERVER_HOST => $server->host,
			                  ));
		}

		$responseServers = $gameQ->requestData();
		foreach($responseServers as $index => $responseServer) {
			CronController::print_pretty("Server: " . $servers[$index]->title);
			CronController::print_pretty("--------" . ($responseServer['gq_online'] ? "online" : "offline"));
			$sqlToUpdate = [ ];

			$sqlToUpdate[GameMonitoring::DB_ROW_CACHE_PLAYERS]    = $responseServer['gq_numplayers'];
			$sqlToUpdate[GameMonitoring::DB_ROW_CACHE_MAXPLAYERS] = $responseServer['gq_maxplayers'];

			if($servers[$index]->cache_state != 'techworks') {
				$sqlToUpdate[GameMonitoring::DB_ROW_REQUEST_TIMES]    = $servers[$index]->request_times + 1;
				$sqlToUpdate[GameMonitoring::DB_ROW_CACHE_STATE] = $responseServer['gq_online'] ? 'online' : 'offline';
				if($responseServer['gq_online']) {
					$sqlToUpdate[GameMonitoring::DB_ROW_REQUEST_ONLINE] = $servers[$index]->request_online + 1;
				} else {
					$sqlToUpdate[GameMonitoring::DB_ROW_LAST_OFFLINE] = date("Y-m-d H:i:s", NOW);
				}
			}

			if($responseServer['gq_numplayers'] > $servers[$index]->historical_players) {
				$sqlToUpdate[GameMonitoring::DB_ROW_HISTORICAL_PLAYERS] = $responseServer['gq_numplayers'];
				$sqlToUpdate[GameMonitoring::DB_ROW_HISTORICAL_DATE]    = date("Y-m-d H:i:s", NOW);
			}

			DB::table(GameMonitoring::DB_CACHE)->insert([
				                                            'server_id'   => $index,
				                                            'num_players' => $responseServer['gq_numplayers'],
				                                            'time'        => NOW
			                                            ]);

			DB::table(GameMonitoring::DB_LIST)
			  ->where(GameMonitoring::DB_ROW_ID, '=', $index)
			  ->update($sqlToUpdate);
		}

		return 60; // todo
	}
}