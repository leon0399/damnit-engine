<?php

/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

define('DEBUG', false);
define('STATE', 'development');

define('SEF', true); //Search engine friendly urls
define('LOCALE', 'ru_RU'); //@Todo translations system

define('SYSPATH', ENGPATH . 'System' . DIRECTORY_SEPARATOR);
define('MODULEPATH', 'module' . DIRECTORY_SEPARATOR);
define('THEMEPATH', 'view' . '/' . 'Theme' . '/');

define('SITE_PATH', '');

define("NOW", time());

class Constants {

	public static function pre_config($path) {
		define('SALT', 'd4F54@4ed!!ef');
		define('UID', 'flgigh83ryjv');

		$ruri = str_replace("index.php", "", $path);
		$duri = $ruri . SITE_PATH;

		if (!SEF) {
			define('RURI', $ruri . '?/'); // example: http://localhost/?/
			define('NRURI', RURI);
		} else {
			define('RURI', $ruri); // example:  http://localhost/
			/**
			 * Ignore Search Engine Friendliness
			 */
			define('NRURI', $ruri . '?/');
		}

		define('DURI', $duri);
	}

	public static function post_config(array $config) {
		define('DBPREFIX', $config['prefix']);
	}

	public static function post_boot($views_dir) {
		define('CURR_THEME', DURI . $views_dir);
		define('CURR_THEME_PATH', THEMEPATH . $views_dir);
	}
}