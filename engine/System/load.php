<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */
namespace DamnIT\Boot;

// include_once __DIR__ .'/vendor/owasp/csrf-protector-php/libs/csrf/csrfprotector.php';

// use DamnIT\Access\CSRFProtect;
use DamnIT\Plugin\PluginManager;
use DamnIT\Route\Route;
use DamnIT\Util\Util;
use Illuminate\Database\Capsule\Manager as Capsule;
use Service\Crypt\Crypton;
use Service\NoSpam\NoSpam;

define('ENGPATH', (((dirname(dirname(__FILE__))))) . DIRECTORY_SEPARATOR);

if (@$_SERVER["HTTPS"] == "on") {
	$protocol = "https://";
} else {
	$protocol = "http://";
}
$path = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];

require('DamnIT' . DIRECTORY_SEPARATOR . 'Boot' . DIRECTORY_SEPARATOR . 'Autoloader.php');
require(ENGPATH . 'Constants.php');
// require(SYSPATH . "/vendor/wixel/gump/gump.class.php");
require(SYSPATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

\Constants::pre_config($path); // Creating reasonable constants before loading config
$autoloader = new Autoloader(); // Registering new SPL_AUTOLOADER

$config = require_once(ENGPATH . 'config.php'); // Config loaded
\Constants::post_config($config);

$capsule = new Capsule();
$capsule->addConnection($config);
$capsule->setAsGlobal();
$capsule->bootEloquent();

if(NoSpam::isIpBanned(NoSpam::getUserRealIP())) {
	header("Content-type: text/plain");
	die("Данный IP-адрес заблокирован! \n This IP address is banned!");
}

Util::start_session();

PluginManager::registerAll();

Util::get_config();
\Constants::post_boot(Util::get_opt('theme') . "/");
$route = new Route();