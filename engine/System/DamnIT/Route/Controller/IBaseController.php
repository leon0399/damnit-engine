<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 22.11.2015
 * Time: 23:46
 */

namespace DamnIT\Route\Controller;


interface IBaseController {
	public function registerRoutes(\DamnIT\Route\Route $router);
}