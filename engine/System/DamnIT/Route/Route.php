<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 22.11.2015
 * Time: 18:15
 */

namespace DamnIT\Route;

use DamnIT\Plugin\BasePlugin;
use DamnIT\Plugin\PluginManager;
use DamnIT\Route\Controller\IBaseController;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Route {

	/**
	 * @type \Slim\Container
	 */
	private $container;

	/**
	 * @type \Slim\App
	 */
	public $router;

	private $registeredControllers = array();

	public function __construct() {
		$this->container = new \Slim\Container();
		$this->container['notFoundHandler'] = function ($c) {
			return function ($request, $response) use ($c) {
				return $c['response']
					->withStatus(404)
					->withHeader('Content-Type', 'text/html')
					->write(\DamnIT\Template\Layout::notFound());
			};
		};
		$this->router = new \Slim\App($this->container);
		$this->loadRouters();
		$this->router->add(function (RequestInterface  $request, ResponseInterface $response, callable $next) {
			$uri = $request->getUri();
			$path = $uri->getPath();
			if ($path != '/' && substr($path, -1) == '/') {
				// permanently redirect paths with a trailing slash
				// to their non-trailing counterpart
				$uri = $uri->withPath(substr($path, 0, -1));
				return $response->withRedirect((string)$uri, 301);
			}

			return $next($request, $response);
		});

		$this->run();
	}

	public function registerController(IBaseController $controller) {
		array_push($this->registeredControllers, $controller);
	}

	public function loadRouters() {
		$routes = array_diff(
				scandir(
						dirname(ENGPATH) . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR
						. 'Controllers'
				), array( '..', '.' )
		);
		//var_dump($routes);
		foreach($routes as $router) {
			$dir = dirname(ENGPATH) . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR
			       . 'Controllers' . DIRECTORY_SEPARATOR . $router;
			if(is_dir($dir)) {
				continue;
			}
			require_once $dir;
			$router = str_replace('.php', '', $router);
			$router = "\\Controllers\\" . $router;
			$router = new $router();
			if($router instanceof IBaseController) {
				$this->registerController($router);
			}
		}

		/** @type BasePlugin $plugin */
		foreach(PluginManager::$plugins as $plugin) {
			$this->registerController($plugin);
		}
	}

	public function group($mvc, $func) {
		return $this->router->group($mvc, $func);
	}

	public function get($mvc, $func) {
		return $this->router->get($mvc, $func);
	}

	public function post($mvc, $func) {
		return $this->router->post($mvc, $func);
	}

	public function put($string, $param) {
		return $this->router->put($string, $param);
	}

	public function run() {
		/** @type IBaseController $controller */
		foreach ($this->registeredControllers as $controller) {
			$controller->registerRoutes($this);
		}

		return $this->router->run();
	}

	public function getContainer() {
		return $this->router->getContainer();
	}
}