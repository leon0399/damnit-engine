<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 23:31
 */

namespace DamnIT\Hook;

use Smarty_Internal_Template;

class HookSystem {
	public static $hooks = [ ];

	/**
	 * @param $name
	 * @param Smarty_Internal_Template $template
	 */
	public static function callHook($name, $template) {
		// var_dump($name);
		if(!isset(self::$hooks[$name])) return;

		foreach(self::$hooks[$name] as $hook) {
			return $hook($template);
		}
	}

	public static function addHook($name, callable $callback) {
		if(!isset(self::$hooks[$name])) {
			self::$hooks[$name] = [];
		}
		array_push(self::$hooks[$name], $callback);
	}
}