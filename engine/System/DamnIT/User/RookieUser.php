<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 26.11.2015
 * Time: 23:05
 */

namespace DamnIT\User;


use DamnIT\Util\Util;

class RookieUser {
	/**
	 * RookieUser constructor.
	 *
	 * @param $data array
	 */
	public function __construct($data) {
		$this->username = $data['username'];
	}
}