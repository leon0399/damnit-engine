<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 19.11.2015
 * Time: 22:50
 */

namespace DamnIT\User;


use DamnIT\Role\Role;
use DamnIT\Util\UUID;
use Service\NoSpam\NoSpam;

class GuestUser extends BaseUser {

	/**
	 * GuestUser constructor.
	 * @override
	 */
	public function __construct() {
		parent::__construct(new GuestUserData());
	}

	public function loggedIn() {
		return false;
	}

	public function getUUID() {
		return UUID::UNKNOWN;
	}
}

class GuestUserData extends \stdClass {
	public $user_uuid_text = UUID::UNKNOWN;
	public $role_id = 1;
	public $username = "Guest";
	public $email = "";
	public $gender = 0;
	public $register_date;
	public $register_ip;
	public $last_active_date;
	public $last_active_ip;
	public $message_count = 0;
	public $like_count = 0;
	public $report_count = 0;
	public $warn_count = 0;

	public function __construct() {
		$this->register_date = date("Y-m-d H:i:s", 0);
		$this->register_ip = ip2long(NoSpam::getUserRealIP());
		$this->last_active_date = date("Y-m-d H:i:s", 0);
		$this->last_active_ip = ip2long(NoSpam::getUserRealIP());
	}
}