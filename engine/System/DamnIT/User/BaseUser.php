<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 19.11.2015
 * Time: 22:48
 */

namespace DamnIT\User;


use DamnIT\Role\Role;
use DamnIT\Role\IRoledEntity;
use Illuminate\Support\Facades\DB;

class BaseUser implements IUser, IRoledEntity {

	/**
	 * @type object
	 */
	protected $uuid;
	/**
	 * @type String
	 */
	protected $username;
	/**
	 * @type String
	 */
	protected $email;

	/**
	 * @type Role
	 */
	protected $role;

	/**
	 * @type integer
	 */
	protected $gender;

	/**
	 * @type integer
	 */
	protected $registerDate;

	/**
	 * @type String
	 */
	protected $registerIp;

	/**
	 * @type integer
	 */
	protected $lastActiveDate;

	/**
	 * @type String
	 */
	protected $lastActiveIp;

	/**
	 * @type integer
	 */
	protected $messageCount;

	/**
	 * @type integer
	 */
	protected $likeCount;

	/**
	 * @type integer
	 */
	protected $reportCount;

	/**
	 * @type integer
	 */
	protected $warnCount;

	public function __construct($db) {
		$this->uuid = $db->user_uuid_text;
		$this->role = new Role($db->role_id);
		$this->username = $db->username;
		$this->email = $db->email;
		$this->gender = $db->gender;
		$this->registerDate = $db->register_date;
		$this->registerIp = long2ip($db->register_ip);
		$this->lastActiveDate = $db->last_active_date;
		$this->lastActiveIp = long2ip($db->last_active_ip);
		$this->messageCount = $db->message_count;
		$this->likeCount = $db->like_count;
		$this->reportCount = $db->report_count;
		$this->warnCount = $db->warn_count;
	}

	/**
	 * @return int
	 */
	public function getUUID() {
		return $this->uuid;
	}

	/**
	 * @return String
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @return String
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return \DamnIT\Role\Role
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * Check if user has a specific privilege
	 *
	 * @param $perm
	 *
	 * @return bool
	 */
	public function hasPerm($perm) {
		return $this->role->hasPerm($perm);
	}

	public function loggedIn() {
		return false;
	}

	/**
	 * @param bool|false $lessPrivacy
	 *
	 * @return array
	 */
	public function getPublicData($lessPrivacy = false) {
		$public = array(
			'uuid'            => $this->getUUID(),
			'username'       => $this->getUsername(),
			'role'           => array(
				'id'          => $this->getRole()->getID(),
				'name'        => $this->getRole()->getName(),
				'permissions' => $this->getRole()->getPerms()
			),
			'permissions'    => $this->getRole()->getPerms(),
			'gender'         => $this->gender,
			'registerDate'   => $this->registerDate,
			'lastActiveDate' => $this->lastActiveDate,
			'messageCount'   => $this->messageCount,
			'likeCount'      => $this->likeCount,
			'reportCount'    => $this->reportCount,
			'warnCount'      => $this->warnCount,
		);
		if($lessPrivacy) {
			$public['email'] = $this->getEmail();
		}

		return $public;
	}
}