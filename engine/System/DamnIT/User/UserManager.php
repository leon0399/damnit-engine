<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 20.11.2015
 * Time: 23:04
 */

namespace DamnIT\User;

use Illuminate\Database\Capsule\Manager as DB;

class UserManager {
	/**
	 * @type BaseUser
	 */
	public static $clientUser;

	public static function Get($id = 0) {
		$loggedIn = isset($_SESSION[UID . 'USER']['uuid']) && $_SESSION[UID . 'USER']['uuid'] !== "0";
		if(!$id) {
			if($loggedIn) {
				$id = $_SESSION[UID . 'USER']['uuid']; //todo
			} else {
				return new GuestUser(); //guest
			}
		}
	}

	/**
	 * @param $row
	 * @param $value
	 *
	 * @return BaseUser|mixed
	 */
	public static function GetUser($row, $value) {
		$triggers = array(
			'uuid'      => 'GetUserByUUID',
			'user_uuid' => 'GetUserByUUID',

			'username' => 'GetUserByUsername',
			'login'    => 'GetUserByUsername',
			'nickname' => 'GetUserByUsername',

			'email'  => 'GetUserByEmail',
			'mail'   => 'GetUserByEmail',
			'e-mail' => 'GetUserByEmail',
		);
		if(!empty($triggers[strtolower($row)])) {
			return call_user_func(array( 'DamnIT\User\UserManager', $triggers[strtolower($row)] ), $value);
		} else {
			return false;
		}
	}

	/**
	 * @param $uuid string
	 *
	 * @return bool|\DamnIT\User\BaseUser
	 */
	public static function GetUserByUUID($uuid) {
		$packed = pack('h*', str_replace('-', '', $uuid));
		$user   = self::GetUserObject(array( 'user_uuid_bin' => $packed ));
		if(!$user) {
			return false; //wrong uuid passed
		}

		return new BaseUser($user);
	}

	/**
	 * @param $username
	 *
	 * @return bool|\DamnIT\User\BaseUser
	 */
	public static function GetUserByUsername($username) {
		$user = self::GetUserObject(array( 'username' => $username ));
		//var_dump($user);
		if(!$user) {
			return false; //wrong username passed
		}

		return new BaseUser($user);
	}

	/**
	 * @param $email
	 *
	 * @return bool|\DamnIT\User\BaseUser
	 */
	public static function GetUserByEmail($email) {
		$user = self::GetUserObject(array( 'email' => $email ));
		if(!$user) {
			return false; //wrong email passed
		}

		return new BaseUser($user);
	}

	/**
	 * @param array $reqs
	 *
	 * @return array|bool
	 */
	protected static function GetUserObject(array $reqs) {
		$query = DB::table('users')->select('user_uuid_text',
		                                    'role_id',
		                                    'user_state',
		                                    'username',
		                                    'email',
		                                    'gender',
		                                    'register_date',
		                                    'register_ip',
		                                    'last_active_date',
		                                    'last_active_ip',
		                                    'message_count',
		                                    'like_count',
		                                    'report_count',
		                                    'warn_count');
		foreach($reqs as $key => $val) {
			$query->where($key, $val);
		}

		return $query->first();
	}

	public static function GetAllUsers() {
		$query = DB::table('users')->select('user_uuid_text',
		                                    'role_id',
		                                    'username',
		                                    'gender',
		                                    'register_date',
		                                    'last_active_date',
		                                    'message_count',
		                                    'like_count',
		                                    'report_count')->orderBy('register_date', 'asc')->get();

		return $query;
	}
}