<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 19.11.2015
 * Time: 23:02
 */

namespace DamnIT\User;


interface IUser {
	public function loggedIn();

	public function getUUID();
	public function getUsername();
	public function getEmail();
}