<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

namespace DamnIT\Store;



class Store {
	private static $storedItems = array();

	public static function storeSet($key, $val = false) {
		self::$storedItems[$key] = $val;
	}

	public static function storeGet($key, $def = false) {
		return isset(self::$storedItems[$key]) ? self::$storedItems[$key] : $def;
	}
}