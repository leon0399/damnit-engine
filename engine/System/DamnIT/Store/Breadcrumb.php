<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

namespace DamnIT\Store;


class Breadcrumb {
	private static $map = array();

	public static function add($key, $url) {
		//self::$map[$key] = $url;
		array_unshift(self::$map, array('title' => $key,
		                                'url'   => $url));
	}

	public static function get() {
		return self::$map;
	}
}