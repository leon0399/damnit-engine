<?php

/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */
namespace DamnIT\Util;

use Memcached;

class Util {
	private static $confUtil = array(
		'sessionHandler' => 'default'
	);

	private static $options = array();

	public static function start_session() {
		switch(self::$confUtil['sessionHandler']) {
			case 'memcached':
				if (!extension_loaded('memcached') || (!class_exists('Memcached'))) {
					die('Memcached extension not installed');
				}
				$cache = new Memcached();
				$cache->addServer('127.0.0.1', 11211);
				$session = new \DamnIT\Session\MemcachedSessionHandler($cache);
				break;
			default:
				break;
		}
		session_start();
	}

	/**
	 * ${DESCRIPTION}
	 *
	 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru)
	 */
	public static function get_config() {
		$conf = \DB::table('config')->get();
		$info = array();
		foreach ($conf as $c) {
			$info[$c->option_name] = self::valid_JSON($c->option_value);
		}
		self::$options = $info;
	}

	/**
	 * Get config options fro DB
	 * Получить значение конфигурации из БД
	 *
	 * @param $option
	 *
	 * @return string|array if option is JSON
	 */
	public static function get_opt($option) {
		if (empty(self::$options) || !isset(self::$options[$option])) {
			return 'The option \'' . $option . '\' does not exist in the table';
		} else {
			return self::$options[$option];
		}
	}

	/**
	 *
	 * Validates json
	 *
	 * @param   string          $str
	 * @return  string|array
	 *
	 * returns decoded json if valid string else returns the string itself
	 */
	public static function valid_JSON($str) {

		//we want associative array so pass true
		$json = json_decode($str, true);

		if ($json == null) {
			return $str;
		} else {
			return $json;
		}
	}
}