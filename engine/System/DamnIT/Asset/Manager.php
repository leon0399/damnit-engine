<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 15.11.2015
 * Time: 13:45
 */

namespace DamnIT\Asset;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Assetic\AssetWriter;
use Assetic\Filter\CssMinFilter;
use Assetic\Filter\LessFilter;

class Manager {
	private static $instance;

	private static $cssFiles = array();
	private static $jsFiles  = array();
	private static $defers   = array();

	public function __construct() {
	}

	public static function addCSS($link) {
		if(substr($link, 0, 4) == 'http') {

		} else {
			$link = DURI . CURR_THEME_PATH . 'css/' . $link . '.css';
		}
		array_push(self::$cssFiles, $link);
	}

	public function dumpCSS() {
		return self::$cssFiles;
	}

	public static function addJS($link) {
		if(substr($link, 0, 4) == 'http') {

		} else {
			$link = DURI . CURR_THEME_PATH . 'js/' . $link . '.js';
		}
		array_push(self::$jsFiles, $link);
	}

	public function dumpJS() {
		return self::$jsFiles;
	}

	public static function addDefer($link) {
		if(substr($link, 0, 4) == 'http') {

		} else {
			$link = DURI . CURR_THEME_PATH . 'js/' . $link . '.js';
		}
		array_push(self::$defers, $link);
	}

	public static function dumpDefer() {
		return self::$defers;
	}
}