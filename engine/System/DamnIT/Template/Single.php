<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

namespace DamnIT\Template;


class Single {
	private $smarty = null;

	public function __construct($path = CURR_THEME_PATH) {

		$this->smarty = new \Smarty();
		$this->parent = $this->smarty->parent;
		$this->load($path);
	}

	public function load($path) {
		$cache_dir = 'cache/smarty/';
		$this->smarty->setTemplateDir($path . 'templates');

		$this->smarty->setCompileDir($cache_dir . 'templates_c/');
		$this->smarty->setCacheDir($cache_dir . 'cache/');
		$this->smarty->setConfigDir($path . 'configs/');
		$this->smarty->addPluginsDir(SYSPATH . DIRECTORY_SEPARATOR . 'DamnIT' . DIRECTORY_SEPARATOR . 'Template' .
		                             DIRECTORY_SEPARATOR . 'SmartyPlugins/');

		//self::$smarty->caching = 1;
		$this->smarty->debugging = DEBUG;
		//$this->caching = \Smarty::CACHING_LIFETIME_CURRENT;
	}

	public function assign($var, $value = false) {

		if (!$value) {
			$this->smarty->assign($var);
		} else {
			$this->smarty->assign($var, $value);
		}
	}

	public function display($filename) {
		$this->smarty->display($filename);
	}

	public function fetch($filename) {
		return $this->smarty->fetch($filename);
	}

	public function getTemplateDir() {
		return $this->smarty->getTemplateDir();
	}

	public function createTemplate($file, $smarty) {
		return $this->smarty->createTemplate($file, $smarty);
	}
}