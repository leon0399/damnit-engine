<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 23:24
 */
use DamnIT\Hook\HookSystem;

/**
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 */
function smarty_function_hook($params, $template) {
	if (empty($params['name'])) {
		user_error("[plugin] fetch parameter 'name' cannot be empty", E_USER_NOTICE);

		return;
	}
	return HookSystem::callHook($params['name'], $template);
}