<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

namespace DamnIT\Template;

use DamnIT\Asset\Manager;
use DamnIT\Store\Breadcrumb;
use DamnIT\Store\Store;
use DamnIT\User\UserManager;
use DamnIT\Util\Util;

use Illuminate\Database\Capsule\Manager as DB;

class Layout {
	/**
	 * @type \Smarty
	 */
	private static $template;

	public static function load($tpl, $css_files = array(), $js_files = array()) {
		require CURR_THEME_PATH . 'theme.php';

		Breadcrumb::add('Главная', '');
		$asset               = new Manager();
		$site['head']['css'] = $asset->dumpCSS();
		$site['body']['js']  = $asset->dumpJS();
		$site['defers']      = json_encode($asset->dumpDefer(), JSON_UNESCAPED_SLASHES | JSON_HEX_AMP);

		self::$template = self::getTemplate();

		self::$template->assign('site', $site);

		self::$template->assign('sub_title', Store::storeGet('sub-title'));
		self::$template->assign('site_title', Util::get_opt('site-title'));
		self::$template->assign('breadcrumb', Breadcrumb::get());

		self::$template->assign('social', Util::get_opt('social-links'));
		$ld_json = array(
				'@context'        => 'http://schema.org',
				'@type'           => 'Organisation',
				'name'            => Util::get_opt('site-title'),
				'url'             => RURI,
				'logo'            => DURI . 'public/Download/img/logo/logo.250.png',
				'sameAs'          => Util::get_opt('social-links'),
				/*'potentialAction' => array(
						'@type'       => 'SearchAction',
						'target'      => RURI . 'search?q={search_term_string}',
						'query-input' => 'required name=search_term_string'
				)*/
		);
		self::$template->assign('ld_json', json_encode($ld_json, JSON_UNESCAPED_SLASHES));
		self::$template->assign('site_description', Util::get_opt('site-description'));
		self::$template->assign('site_keywords', Util::get_opt('site-keywords'));
		self::$template->assign('menu_items', DB::table("dynamic_bar_menu")->where("active", "=", 1)->orderBy("order")
			->get());

		$I = UserManager::Get();
		self::$template->assign('I', $I);

		$html = self::$template->fetch("$tpl.tpl");

		return $html;
	}

	public static function notFound() {
		Store::storeSet('sub-title', '404 Не Найдено');
		Breadcrumb::add('404 Не Найдено', '404');
		return self::load('errors/404');
	}

	public static function getTemplate() {
		if(!self::$template) {
			self::$template = new Single();
		}

		return self::$template;
	}


}