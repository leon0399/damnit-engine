<?php


namespace DamnIT\Boot;

use Illuminate\Support\ClassLoader;

require(ENGPATH . 'Service/Loader/Autoloader.php');
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */
class Autoloader {
	private static $_lastLoadedFilename;

	public function __construct($log = false) {
		ClassLoader::register();

		$loader = new \Service\Loader\Autoloader();
		$loader->register();
		//\Illuminate\Support\ClassLoader::register();

		//spl_autoload_register(array($this, 'loadPackages'));

		//$aliaser = new AliasLoader();
		//$aliaser->register();

		//\DamnIT\Boot\ModuleLoader::register();
	}

	/*
	public static function loadPackages($class) {

		$className = explode('\\', $class);

		$class = array_pop($className);
		$namespace = implode('/', $className);

		if(strpos($namespace, 'Assetic') !== false) {
			$namespace = 'Ext/assetic-master/src/' . $namespace;
		}

		self::$_lastLoadedFilename = SYSPATH . $namespace . "/" . $class . '.php';
		if (is_file(self::$_lastLoadedFilename)) {
			require self::$_lastLoadedFilename;
		}
	}
	*/
}