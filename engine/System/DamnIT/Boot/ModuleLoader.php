<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 17.11.2015
 * Time: 21:42
 */

namespace DamnIT\Boot;

/**
 * Class ModuleLoader
 * @package DamnIT\Boot
 * @deprecated
 */
class ModuleLoader {
	private static $_lastLoadedFilename;

	public static function loadModules($class) {

		$className = explode('\\', $class);

		$class = array_pop($className);
		$namespace = implode('/', $className);

		self::$_lastLoadedFilename = 'module/' . $namespace . "/" . $class . '.php';
		if (is_file(self::$_lastLoadedFilename)) {
			require self::$_lastLoadedFilename;
		}
	}

	public static function register() {
		spl_autoload_register(array('DamnIT\Boot\ModuleLoader', 'loadModules'));
	}
}