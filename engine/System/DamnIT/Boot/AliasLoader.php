<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

namespace DamnIT\Boot;

/**
 * Class AliasLoader
 * @package DamnIT\Boot
 */
class AliasLoader {

	protected $aliases = array(
		"DB" => "Illuminate\Database\Capsule\Manager",
	);

	/**
	 * Should the autoloader function be prepended instead of appended
	 */
	const PREPEND = TRUE;

	/**
	 * Should the autoloader throw error
	 */
	const THROW_ERR = TRUE;


	public function register() {

		/**
		 * Register the alias loader function to the PHP autloader
		 */
		spl_autoload_register(array($this, 'alias_loader'), self::THROW_ERR, self::PREPEND);
	}

	/**
	 * Adds an alias at runtime
	 * @param type $alias
	 */
	public function alias_loader($alias) {

		if(isset($this->aliases[$alias])) {

			class_alias($this->aliases[$alias], $alias);
		}
	}

	/**
	 * Adds an alias to a namespace
	 *
	 * @param string $namespace
	 * @param string $alias
	 */
	public function add($namespace, $alias) {

		$this->aliases[$alias] = $namespace;
	}

	/**
	 * Removes an alias to a namespace
	 *
	 * @param type $alias
	 */
	public function remove($alias) {

		unset($this->aliases[$alias]);
	}
}