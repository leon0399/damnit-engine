<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 22:09
 */

namespace DamnIT\Plugin;
use Illuminate\Database\Capsule\Manager as DB;

class PluginManager {
	/**
	 * @type array
	 */
	public static $plugins = [];
	public static function registerAll() {
		$sqlPlugins = DB::table("installed_plugins")->where('active', '=', 1)->get();
		foreach($sqlPlugins as $sqlPlugin) {
			self::$plugins[$sqlPlugin->plugin_name] = new $sqlPlugin->plugin_class;
		}
	}
}