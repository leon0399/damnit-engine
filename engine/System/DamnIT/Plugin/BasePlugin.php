<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.01.2016
 * Time: 16:28
 */

namespace DamnIT\Plugin;

use DamnIT\Route\Controller\IBaseController;

abstract class BasePlugin implements IBaseController {
	public function registerRoutes(\DamnIT\Route\Route $router) {

	}
}