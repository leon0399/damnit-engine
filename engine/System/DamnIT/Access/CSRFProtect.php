<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 17.11.2015
 * Time: 23:14
 */

namespace DamnIT\Access;

/**
 * Class CSRFProtect
 * @package DamnIT\Access
 * @deprecated
 */
class CSRFProtect {

	private static $config = array(
		'tokenLength' => 128,
		'logDirectory' => 'cache/log',

	);
	private static $cookieExpiryTime = 1800; //30min

	public static function protect() {
		if (getenv('mod_csrfp_enabled'))
			return;

		if (!isset($_COOKIE['csrfp'])
		    || !isset($_SESSION['csrfp'])
		    || !is_array($_SESSION['csrfp'])
		    || !in_array($_COOKIE['csrfp'],
		                 $_SESSION['csrfp']))
			self::refreshToken();

		self::authorize();
	}

	private static function authorize() {
		if($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!(isset($_POST['csrfp'])
			      && isset($_SESSION['csrfp'])
			      && (self::isValidToken($_POST['csrfp']))
			)) { //action in case of failed validation
				self::failedValidationAction();
			} else { //refresh token for successfull validation
				self::refreshToken();
			}
		}
	}

	private static function isValidToken($token) {
		if (!isset($_SESSION['csrfp'])) return false;
		if (!is_array($_SESSION['csrfp'])) return false;
		foreach ($_SESSION['csrfp'] as $key => $value) {
			if ($value == $token) {
				// Clear all older tokens assuming they have been consumed
				foreach ($_SESSION['csrfp'] as $_key => $_value) {
					if ($_value == $token) break;
					array_shift($_SESSION['csrfp']);
				}
				return true;
			}
		}
		return false;
	}

	private static function failedValidationAction()
	{

	}

	public static function refreshToken()
	{
		$token = self::generateAuthToken();
		if (!isset($_SESSION['csrfp'])
		    || !is_array($_SESSION['csrfp']))
			$_SESSION['csrfp'] = array();
		//set token to session for server side validation
		array_push($_SESSION['csrfp'], $token);
		//set token to cookie for client side processing
		setcookie('csrfp',
		          $token,
		          time() + self::$cookieExpiryTime);
	}

	public static function generateAuthToken()
	{
		/*//if config tokenLength value is 0 or some non int
		if (intval(self::$config['tokenLength']) == 0) {
			self::$config['tokenLength'] = 32;	//set as default
		}
		//#todo - if $length > 128 throw exception
		if (function_exists("hash_algos") && in_array("sha512", hash_algos())) {
			$token = hash("sha512", mt_rand(0, mt_getrandmax()));
		} else {
			$token = '';
			for ($i = 0; $i < 128; ++$i) {
				$r = mt_rand(0, 35);
				if ($r < 26) {
					$c = chr(ord('a') + $r);
				} else {
					$c = chr(ord('0') + $r - 26);
				}
				$token .= $c;
			}
		}
		return substr($token, 0, self::$config['tokenLength']);
		*/
		return base64_encode(openssl_random_pseudo_bytes(self::$config['tokenLength']));
	}
}