<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 19.11.2015
 * Time: 22:57
 */

namespace DamnIT\Role;


interface IRoledEntity {
	public function hasPerm($perm);
}