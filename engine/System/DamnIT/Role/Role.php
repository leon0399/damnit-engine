<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 19.11.2015
 * Time: 23:07
 */

namespace DamnIT\Role;

use Illuminate\Database\Capsule\Manager as DB;

class Role {

	/**
	 * @type integer
	 */
	private $roleID;

	/**
	 * @type String
	 */
	private $roleName;

	/**
	 * @type array
	 */
	protected $permissions = array();

	/**
	 * @type \DamnIT\Role\Role
	 */
	private $parent;

	public function __construct($role) {
		if($role < 1) {
		} //@Todo handler if $role < 1

		$tmpData        = DB::table('roles')
		                    ->select(array( 'role_id', 'parent_id', 'role_name', ))
		                    ->where(array(
				                            'role_id' => $role,
		                            ))
		                    ->first();
		$this->roleID   = $tmpData->role_id;
		$this->roleName = $tmpData->role_name;
		$this->parent   = $tmpData->parent_id;
		if($this->parent !== null) {
			$this->parent = new Role($this->parent);
		}

		$this->initRolePerms();
	}

	protected function initRolePerms() {
		if($this->parent !== null) {
			$this->permissions = $this->parent->getPerms();
		} // @fixme 12.01.2016: crash

		foreach(DB::table('roles_perms')->where('role_id', $this->roleID)->pluck('permission') as $role) {
			$this->permissions[$role] = true;
		}
	}

	/**
	 * check if a permission is set
	 *
	 * @param $permission
	 *
	 * @return bool
	 */
	public function hasPerm($permission) {
		return isset($this->permissions[$permission]) ? true : false;
	}

	public function getPerms() {
		return $this->permissions;
	}

	public function getID() {
		return $this->roleID;
	}

	public function getName() {
		return $this->roleName;
	}
}