<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 23.11.2015
 * Time: 0:28
 */

namespace DamnIT\Api;


use DamnIT\Api\Misc\ApiCollection;
use DamnIT\User\BaseUser;
use DamnIT\User\RookieUser;
use DamnIT\User\UserManager;

class UsersApi {

	/**
	 * @type ApiCollection
	 */
	protected $collection;

	public function __construct(array $args = array()) {
		$this->collection = new ApiCollection();
		$this->gump = new \GUMP();
		$this->args = $this->gump->sanitize($args);
	}

	public function getByUUID() {
		$this->gump->validation_rules(array(
			                              'uuid' => 'required|alpha_dash',
		                              ));
		$this->gump->filter_rules(array(
			                          'uuid' => 'trim',
		                          ));
		$validated_data = $this->gump->run($this->args);

		if($validated_data === false) {
			$this->collection->setStatus(400);
		} else {
			/** @type BaseUser $user */
			$user = UserManager::GetUser('uuid', $validated_data['uuid']);
			if($user === false) {
				$this->collection->setStatus(404);
			} else {
				$this->collection->setStatus(200);
				$this->collection['user'] = $user->getPublicData();
			}
		}
	}

	public function getByUsername() {
		$this->gump->validation_rules(array(
			                              'username' => 'required|alpha_dash',
		                              ));
		$this->gump->filter_rules(array(
			                          'username' => 'trim|sanitize_string',
		                          ));
		$validated_data = $this->gump->run($this->args);

		if($validated_data === false) {
			$this->collection->setStatus(400);
		} else {
			/** @type BaseUser $user */
			$user = UserManager::GetUser('username', $validated_data['username']);
			// var_dump($user);
			if($user === false) {
				$this->collection->setStatus(404);
			} else {
				$this->collection->setStatus(200);
				$this->collection['user'] = $user->getPublicData();
			}
		}
	}

	public function getByEmail() {
		$this->gump->validation_rules(array(
			                              'email' => 'required|valid_email',
		                              ));
		$this->gump->filter_rules(array(
			                          'email' => 'trim|sanitize_email',
		                          ));
		$validated_data = $this->gump->run($this->args);

		if($validated_data === false) {
			$this->collection->setStatus(400);
		} else {
			/** @type BaseUser $user */
			$user = UserManager::GetUser('email', $validated_data['email']);
			if($user === false) {
				$this->collection->setStatus(404);
			} else {
				$this->collection->setStatus(200);

				$this->collection['user'] = $user->getPublicData(true);
			}
		}
	}

	public function register($request) {
		/*$this->gump->validation_rules(array(
			                              'username'        => 'required|alpha_numeric' . '|regex,/^'
			                                                   . Util::get_opt('username-regex') . '$/',
			                              'email'           => 'required|valid_email' . '|regex,^/'
			                                                   . Util::get_opt('email-regex') . '$/',
			                              'email_repeat'    => 'required|valid_email' . '|regex,/^'
			                                                   . Util::get_opt('email-regex') . '$/',
			                              'password'        => 'required|regex,/^' . Util::get_opt('password-regex')
			                                                   . '$/',
			                              'password_repeat' => 'required|regex,/^' . Util::get_opt('password-regex')
			                                                   . '$/',
			                              'gender'          => 'required|numeric|max_len,1',
			                              'captcha'         => 'required|alpha_dash',
		                              ));*/
		$this->gump->validation_rules(array(
			                              'username'        => 'required|alpha_numeric|min_len,4|max_len,32',
			                              'email'           => 'required|valid_email',
			                              'email_repeat'    => 'required|valid_email',
			                              'password'        => 'required|min_len,6|max_len,800',
			                              'password_repeat' => 'required|min_len,6|max_len,800',
			                              'gender'          => 'required|numeric|max_len,1',
			                              'captcha'         => 'required',
		                              ));
		$this->gump->filter_rules(array(
			                          'username'        => 'trim|sanitize_string',
			                          'email'           => 'trim|sanitize_email',
			                          'email_repeat'    => 'trim|sanitize_email',
			                          'password'        => 'trim',
			                          'password_repeat' => 'trim',
			                          'gender'          => 'trim',
			                          'captcha'         => 'trim',
		                          ));
		$validated_data = $this->gump->run($request);
		if($validated_data === false) {
			$this->collection['errors'] = $this->gump->get_errors_array(true);
			return;
		}
		$rookie = new RookieUser($validated_data['username']);
		//$this->collection
	}

	public function getResponse() {
		return $this->collection->getMessage();
	}

	public function getAll() {
		$startTime = microtime();
		$this->collection["list"] = UserManager::GetAllUsers();
		$stopTime = microtime();
		$this->collection["execTime"] = $stopTime - $startTime;
	}
}