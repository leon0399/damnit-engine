<?php

/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 21.01.2016
 * Time: 23:50
 */

namespace Service\NoSpam;

use Illuminate\Database\Capsule\Manager as DB;

class NoSpam {
	public static function getUserRealIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}

		return $ip;
	}

	public static function isIpBanned($getUserRealIP) {
		DB::table('stored_ip')->where([ 'ip' => ip2long($getUserRealIP), 'banned' => true ]);

		return false;
	}
}