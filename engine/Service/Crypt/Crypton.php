<?php

/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 21.11.2015
 * Time: 3:42
 */

namespace Service\Crypt;

class Crypton {

	public static function pbkdf2 ($password, $salt, $rounds = 15000, $keyLength = 32, $hashAlgorithm = 'sha512', $start = 0)
	{
		// Key blocks to compute
		$keyBlocks = $start + $keyLength;

		// Derived key
		$derivedKey = '';

		// Create key
		for ($block = 1; $block <= $keyBlocks; $block ++) {
			// Initial hash for this block
			$iteratedBlock = $hash = hash_hmac($hashAlgorithm,
			                                   $salt . pack('N', $block), $password, true);

			// Perform block iterations
			for ($i = 1; $i < $rounds; $i ++) {
				// XOR each iteration
				$iteratedBlock ^= ($hash = hash_hmac($hashAlgorithm, $hash,
				                                     $password, true));
			}

			// Append iterated block
			$derivedKey .= $iteratedBlock;
		}

		// Return derived key of correct length
		return base64_encode(substr($derivedKey, $start, $keyLength));
	}

	public static function simple($password, $salt, $hashAlgorithm = 'sha256', $iterations = 1, $cost = 12) {
		$options = [
			'cost' => $cost,
			'salt' => $salt,
		];
		$hash = hash_hmac($hashAlgorithm, $password, $salt);
		for($iteration = 0; $iteration < $iterations; $iteration++) {
			//$hash = hash_hmac($hashAlgorithm, password_hash($password, PASSWORD_DEFAULT, $options), $salt);
			$hash = password_hash(hash_hmac($hashAlgorithm, $hash, $salt), PASSWORD_DEFAULT, $options);
		}
		return $hash;
	}
}