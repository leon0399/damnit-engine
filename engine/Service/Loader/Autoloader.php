<?php
/**
 * Created by PhpStorm.
 * User: leon0399
 * Date: 22.11.2015
 * Time: 17:51
 */

namespace Service\Loader;


class Autoloader {

	/**
	 * Should the autoloader function be prepended instead of appended
	 */
	const PREPEND = TRUE;

	/**
	 * Should the autoloader throw error
	 */
	const THROW_ERR = TRUE;
	private static $_lastLoadedFilename;

	private $alias = array();
	private $registered = false;

	public function __construct() {
		self::addAlias("Illuminate\Database\Capsule\Manager", "DB");
	}

	public function addAlias($class, $alias = null) {
		if(is_array($class)) {
			foreach($class as $file=>$name) {
				$this->alias[$name] = $file;
			}
		} elseif($alias!=null) {
			$this->alias[$alias] = $class;
		}
	}

	public function register() {
		if(!$this->registered) {
			spl_autoload_register(array( $this, 'aliasLoader' ), self::THROW_ERR, self::PREPEND);
			spl_autoload_register(array( $this, 'serviceLoader' ));
			spl_autoload_register(array( $this, 'packageLoader' ));
			spl_autoload_register(array( $this, 'moduleLoader' ));
			$this->registered = true;
		}
	}

	public function aliasLoader($class) {
		if(isset($this->alias[$class])) {
			class_alias($this->alias[$class], $class);
		}
	}

	public function serviceLoader($class) {
		$className = explode('\\', $class);

		$class = array_pop($className);
		$namespace = implode(DIRECTORY_SEPARATOR, $className);

		self::$_lastLoadedFilename = ENGPATH . $namespace . DIRECTORY_SEPARATOR . $class . '.php';
		if (is_file(self::$_lastLoadedFilename)) {
			require self::$_lastLoadedFilename;
		}
	}

	public function packageLoader($class) {
		$className = explode('\\', $class);

		$class = array_pop($className);
		$namespace = implode(DIRECTORY_SEPARATOR, $className);

		self::$_lastLoadedFilename = SYSPATH . $namespace . DIRECTORY_SEPARATOR . $class . '.php';
		if (is_file(self::$_lastLoadedFilename)) {
			require self::$_lastLoadedFilename;
		}
	}

	public function moduleLoader($class) {
		$className = explode('\\', $class);

		$class = array_pop($className);
		$namespace = implode(DIRECTORY_SEPARATOR, $className);

		self::$_lastLoadedFilename = 'module' . DIRECTORY_SEPARATOR . $namespace . DIRECTORY_SEPARATOR . $class . '.php';
		if (is_file(self::$_lastLoadedFilename)) {
			require self::$_lastLoadedFilename;
		}
	}
}