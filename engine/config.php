<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

return array(
	'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => 'damnit_30',
	'username'  => 'root',
	'password'  => '',
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => 'damnit_',
);