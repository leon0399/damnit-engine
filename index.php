<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */

!defined('DAMNIT') or die();
define('DAMNIT', 1);

ini_set('xdebug.var_display_max_depth', '100');
ini_set("display_errors", "on");
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

require_once('engine/System/load.php');