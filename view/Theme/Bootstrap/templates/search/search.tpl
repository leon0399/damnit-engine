{extends file="carcase.tpl"}
{block name="body"}
    <div class="search input-group" role="search" id="mySearch">
        <input type="search" class="form-control" placeholder="Search" id="pgsearch"/>
        <span class="input-group-btn">
            <button class="btn btn-default" type="button">
                <span class="glyphicon glyphicon-search"></span>
                <span class="sr-only">Search</span>
            </button>
        </span>
    </div>
{/block}