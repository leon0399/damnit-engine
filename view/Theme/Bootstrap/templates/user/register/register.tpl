{extends file="carcase.tpl"}
{block name="body"}
    <div class="wizard" data-initialize="wizard" id="myWizard">
        <div class="steps-container">
            <ul class="steps">
                <li data-step="1" class="active">
                    <span class="fa fa-pencil"></span>&nbsp;Заполните данные
                    <span class="chevron"></span>
                </li>
                <li data-step="2">
                    <span class="fa fa-envelope"></span>&nbsp;Активируйте аккаунт
                    <span class="chevron"></span>
                </li>
                <li data-step="3">
                    <span class="fa fa-cogs"></span>&nbsp;Настройка аккаунта
                    <span class="chevron"></span>
                </li>
                <li data-step="4">
                    <span class="fa fa-check"></span>&nbsp;Заполните данные
                </li>
            </ul>
        </div>
        <div class="step-content">
            <div class="step-pane active sample-pane" data-step="1">
                {include file="user/register/steps/step_{$step}.tpl"}
                {block name=panel-body}{/block}
            </div>
            <div class="step-pane sample-pane bg-info alert" data-step="2">
                <h4>Choose Recipients</h4>
                <p>Celery quandong swiss chard chicory earthnut pea potato. Salsify taro catsear garlic gram celery bitterleaf wattle seed collard greens nori. Grape wattle seed kombu beetroot horseradish carrot squash brussels sprout chard. </p>
            </div>
            <div class="step-pane sample-pane bg-danger alert" data-step="3">
                <h4>Design Template</h4>
                <p>Nori grape silver beet broccoli kombu beet greens fava bean potato quandong celery. Bunya nuts black-eyed pea prairie turnip leek lentil turnip greens parsnip. Sea lettuce lettuce water chestnut eggplant winter purslane fennel azuki bean earthnut pea sierra leone bologi leek soko chicory celtuce parsley jÃ­cama salsify. </p>
            </div>
        </div>


    </div>
{/block}
{block name="body"}
    <div class="panel panel-default">
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-rounded">
                    <li role="presentation" {if $step == 1}class="active"{/if}>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Заполните данные">
                            <span class="round-tabs">
                                <i class="fa fa-pencil"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Активируйте аккаунт">
                            <span class="round-tabs">
                                <i class="fa fa-envelope"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Настройка аккаунта">
                            <span class="round-tabs">
                                <i class="fa fa-cogs"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Готово">
                            <span class="round-tabs">
                                <i class="fa fa-check"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel-body" id="registration">
            {include file="user/register/steps/step_{$step}.tpl"}
            {block name=panel-body}{/block}
        </div>
    </div>
{/block}