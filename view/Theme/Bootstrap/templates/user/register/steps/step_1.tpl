{block name=panel-body}
    <form class="form-horizontal reg-form" action="/user/register" method="post" id="reg-form">
        <fieldset>
            <!-- Form Name -->
            <legend>Регистрация</legend>

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-group" id="username-group">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                <input id="reg-username" name="username" type="text" placeholder="Логин" class=" form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="btn-group input-group selectlist" data-initialize="selectlist" id="selectbasic">
                                <span class="input-group-addon"><i class="fa fa-venus-mars fa-fw"></i></span>
                                <button class="btn btn-default dropdown-toggle " data-toggle="dropdown" type="button" style="width: 100%">
                                    <span class="selected-label">Пол</span>
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li data-value="0"><a href="#">Неопределённый</a></li>
                                    <li data-value="1"><a href="#">Мужской</a></li>
                                    <li data-value="2"><a href="#">Женский</a></li>
                                </ul>
                                <input class="hidden hidden-field" name="gender" readonly="readonly" aria-hidden="true" type="text" title="Пол" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-group" id="email-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                <input id="reg-email" name="email" type="email" placeholder="E-mail" class=" form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                <input id="reg-email-repeat" name="email-repeat" type="email" placeholder="E-mail (ещё раз)" class=" form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                <input id="reg-password" name="password" type="password" placeholder="Пароль" class=" form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                <input id="reg-password-repeat" name="password-repeat" type="password" placeholder="Пароль (ещё раз)" class=" form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="progress">
                        <div id="passStrengthBar" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >

                        </div>
                    </div>
                </div>
                {$recaptcha}
            </div>
            <div class="control-group">
                <div class="controls">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary btn-block">Регистрация</button>
                </div>
            </div>
        </fieldset>
    </form>
{/block}