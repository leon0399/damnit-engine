{block name=carousel}
<div class="carousel-ex" style="background-image: url({$smarty.const.DURI}public/Download/img/carousel/bg/background-morning.jpg)">
    <div id="carousel-head-marketing" class="carousel {*marketing*} slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-head-marketing" data-slide-to="0"></li>
            <li data-target="#carousel-head-marketing" data-slide-to="1"></li>
            <li data-target="#carousel-head-marketing" data-slide-to="2" class="active"></li>
            <li data-target="#carousel-head-marketing" data-slide-to="3"></li>
            <li data-target="#carousel-head-marketing" data-slide-to="4"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item">
                <img src="{$smarty.const.DURI}/public/Download/img/carousel/items/old/1.png" alt="...">
                <div class="carousel-caption">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                </div>
            </div>
            <div class="item">
                <img src="{$smarty.const.DURI}/public/Download/img/carousel/items/old/2.png" alt="...">
                <div class="carousel-caption">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                </div>
            </div>
            <div class="item active">
                <div class="marketing-caption hidden-xs">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                    <a class="btn-primary btn-lg" href="{$smarty.const.RURI}user/register">Регистрация</a>
                </div>
                <div class="carousel-caption hidden visible-xs-block">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                </div>
            </div>
            <div class="item">
                <img src="{$smarty.const.DURI}/public/Download/img/carousel/items/old/3.png" alt="...">
                <div class="carousel-caption">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                </div>
            </div>
            <div class="item">
                <img src="{$smarty.const.DURI}/public/Download/img/carousel/items/old/4.png" alt="...">
                <div class="carousel-caption">
                    <h3>{$site_title}</h3>
                    <p>HiTech, MoReally, Vanilla</p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-head-marketing" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-head-marketing" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
{/block}