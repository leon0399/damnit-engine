{block name=login_dropdown}
<ul id="dropdown-login" class="nav navbar-nav navbar-right">
    <li><p class="navbar-text">Уже зарегистрированы?</p></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Вход <span class="caret"></span></a>
        <ul class="dropdown-menu" id="login-dropdown">
            <li>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback" id="loginGroup">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                <input type="text" class="form-control" name="login" id="inputLogin" aria-describedby="loginGroup" placeholder="Логин / E-mail" required="">
                            </div>
                            <span class="fa form-control-feedback" id="feedback-login" aria-hidden="true"></span>
                        </div>

                        <div class="form-group has-feedback" id="passwordGroup">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                <input type="password" class="form-control" name="password" id="inputPassword" aria-describedby="passwordGroup" placeholder="Пароль" required="">
                            </div>
                            <span class="fa form-control-feedback" id="feedback-password" aria-hidden="true"></span>
                        </div>
                        <div class="help-block text-right"><a href="{$smarty.const.RURI}user/forgot">Забыли пароль?</a></div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-block" id="loginButton">Войти!</button>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="save" type="checkbox"> Запомнить меня
                            </label>
                        </div>
                    </div>
                    <div class="bottom text-center">
                        Нет аккаунта? <a href="{$smarty.const.RURI}user/register"><b>Создай!</b></a>
                    </div>
                </div>
            </li>
        </ul>
    </li>
</ul>
{/block}