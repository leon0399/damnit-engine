<!DOCTYPE html>
<html lang="ru">
<head itemscope itemtype="http://schema.org/WebSite">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{$site_description}">
    <meta name="keywords" content="{$site_keywords}">
    <script type="application/ld+json">{$ld_json}</script>
    <!-- TODO: Style Yandex-Browser Api -->
    <meta name="viewport" content="width=device-width, initial-scale=1, ya-title=#ffffff, ya-dock=#ffffff">
    <title>{block "title"}{$sub_title} :: {$site_title}{/block}</title>

    <!--[if lte IE 8]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20121008/json2.min.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Add to homescreen -->
    <link rel="manifest" href="{$smarty.const.RURI}web_app_manifest.json">

    <!-- Fallback to homescreen for Chrome <39 on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="{$site_title}">
    <link rel="icon" sizes="192x192" href="{$smarty.const.RURI}public/Download/img/logo/logo.192.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#ffffff">
    <meta name="apple-mobile-web-app-title" content="{$site_title}">

    <!-- TODO: Tile icon for iOS (120x120) -->
    <!-- <link rel="apple-touch-icon" href="{$smarty.const.RURI}public/Download/img/logo/logo.192.png"> -->
    <link rel="apple-touch-icon" sizes="76x76" href="{$smarty.const.RURI}public/Download/img/logo/logo.touch.76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{$smarty.const.RURI}public/Download/img/logo/logo.touch.120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{$smarty.const.RURI}public/Download/img/logo/logo.touch.152.png">

    <!-- TODO: Tile icon for Win8 (144x144 + tile color) -->
    <!-- <meta name="msapplication-TileColor" content="#333333"> -->
    <!-- <meta name="msapplication-navbutton-color" content="#333333">
            <!-- <meta name="msapplication-TileImage" content="{$smarty.const.RURI}public/Download/img/logo/logo.144.precomposed.png"> -->
    <!-- <meta name="msapplication-square70x70logo" content="{$smarty.const.RURI}public/Download/img/logo/logo.touch.70.precomposed.png"> -->
    <!-- <meta name="msapplication-square150x150logo" content="{$smarty.const.RURI}public/Download/img/logo/logo.touch.150.precomposed.png"> -->
    <!-- <meta name="msapplication-wide310x150logo" content="icon_widetile.png"> -->

    <!-- Add to Yandex.Tablo -->
    <link rel="yandex-tableau-widget" href="{$smarty.const.RURI}ya-tablo-manifest.json" />

    <meta name="theme-color" content="#ffffff">

    <link rel="shortcut icon" href="{$smarty.const.RURI}public/Download/img/logo/logo.32.png">

    <!-- Page styles -->
    {foreach $site.head.css as $file}<link href="{$file}" rel="stylesheet">{/foreach}
    <script>
        var _defs = {
            url: "{$smarty.const.RURI}",
            duri: "{$smarty.const.DURI}",
            logged_in: "{$I->loggedIn()}",
            uuid: "{$I->getUUID()}",
            time: "{$smarty.const.NOW}"
        }
    </script>
    <script>
        window.addEventListener('beforeinstallprompt', function(e) {
            console.log('beforeinstallprompt Event fired');
            e.userChoice.then(function(choiceResult) {
                console.log(choiceResult.outcome);

                if(choiceResult.outcome == 'dismissed') {
                    console.log('User cancelled Home Screen install');
                }
                else {
                    console.log('User added to Home Screen');
                }
            });
            return false;
        });
    </script>
</head>
<body class="fuelux">
    <header class="navbar navbar-clean navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{$smarty.const.RURI}">{$site_title}</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    {foreach from=$menu_items item=menu_item_now}
                        {if $I->hasPerm($menu_item_now->permissions)}
                            <li><a href="{$menu_item_now->url}">{$menu_item_now->title}</a></li>
                        {/if}
                    {/foreach}
                </ul>
                {if $I->loggedIn()}

                {else}
                    {include file='blocks/dropdown_signup.tpl'}
                    {block name="login_dropdown"}{/block}
                {/if}
            </div>
        </div>
    </header>
    <div class="damnit-body">
        {hook name="before_container"}

        <div class="container damnit">
            <!--div class="container-fluid"-->
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        {hook name="before_breadcrumb"}
                        <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">
                            {foreach from=$breadcrumb key=key item=item name=name}
                                <li {if $smarty.foreach.name.last}class="active"{/if} {if !$smarty.foreach.name.first}itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"{/if}>
                                    {if !$smarty.foreach.name.last}<a {if !$smarty.foreach.name.first}itemprop="item"{/if} href="{$smarty.const.RURI}{$item.url}">{/if}
                                        <span {if !$smarty.foreach.name.first}itemprop="name"{/if}>{$item.title}</span>
                                        {if !$smarty.foreach.name.last}</a>{/if}
                                    {if !$smarty.foreach.name.first}<meta itemprop="position" content="{$smarty.foreach.name.index + 1}" />{/if}
                                </li>
                            {/foreach}
                        </ol>
                        {hook name="after_breadcrumb"}
                        {hook name="before_body"}
                        {block name="body"}{/block}
                        {hook name="after_body"}
                    </div>
                    <div class="col-xs-6 col-md-3">
                        {hook name="before_sidebar"}

                        {hook name="after_sidebar"}
                    </div>
                </div>
            <!--/div-->
        </div>
        {hook name="after_container"}
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">&copy; 2012 - 2016 <a href="{$smarty.const.DURI}">{$site_title}</a><br>
                    <small>
                        <a href="http://leon0399.ru/damnit" target="_blank">DamnIT</a> &copy; 2012 <a href="http://leon0399.ru/">leon0399</a>
                    </small>
                </div>
                <div class="col-sm-4" style="text-align: center">
                    {hook name="footer_center"}
                </div>
                <div class="col-sm-4" style="text-align: center">
                    {hook name="footer_right"}
                </div>
            </div>
        </div>
    </footer>

    {foreach $site.body.js as $file}
        <script src="{$file}"></script>
    {/foreach}
    <script>
        var defers = JSON.parse('{$site.defers}');
        $.each(defers, function(key, val) {
            $('head').append("<script src='" + val + "' ><\/script>");
        })
    </script>
</body>
</html>