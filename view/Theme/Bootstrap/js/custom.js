/**
 * Created by leon0399 on 04.11.2015.
 */

/**
 *
 * @type {{adblockCheck: Function, notice: Function, noticeInfo: Function, noticeSuccess: Function, noticeWarning:
 *     Function, noticeDanger: Function, formGroupNotice: Function, formGroupSuccess: Function, formGroupWarning:
 *     Function, formGroupDanger: Function, postJSON: Function}}
 */
DAMNIT = {
	adblockCheck:       function() {
		if ( typeof(window.google_jobrunner) === "undefined" ) {
			$("#noads") .html("<div class=\"alert alert-danger\" role=\"alert\">\n    <div class=\'adb-text\'>\n        Кажется, мы заметили, что ты используешь плагин, блокирующий рекламу.<br/>\n        Наш проект развивается только засчет денег, полученных с него же, и все те деньги, которые мы получаем, идут на поддержания качества наших серверов.<br/>\n        Пожалуйста, добавь нас в исключения (<a class=\'alert-link\' href=\"\">Как это сделать?</a>).\n    </div>\n</div>")
				.fadeIn(500);
		}
		setTimeout('DAMNIT.adblockCheck()', 60000);
	},

	formGroupNotice:    function(elem, state) {
		var states = {
			success:    ['fa-check',    'has-success'],
			warning:    ['fa-warning',  'has-warning'],
			danger:     ['fa-remove',   'has-error']
		};

		for(var val in states) {
			//console.log('Array \'' + val + '\': ' + states[val] );
			$(elem) .removeClass(states[val][1]);

			$(elem) .find('span.form-control-feedback')
				.removeClass('fa')
				.removeClass(states[val][0]);
		}

		$(elem) .addClass(states[state][1]);

		$(elem) .find('span.form-control-feedback')
			.addClass(states[state][0]);

		$(elem) .find('span.form-control-feedback')
			.removeClass('form-control-feedback')
			.addClass('fa')
			.addClass(states[state][0])
			.addClass('form-control-feedback');
	}, //TODO Object literal
	formGroupSuccess:   function(elem) {
		DAMNIT.formGroupNotice(elem, 'success');
	},
	formGroupWarning:   function(elem) {
		DAMNIT.formGroupNotice(elem, 'warning');
	},
	formGroupDanger:    function(elem) {
		DAMNIT.formGroupNotice(elem, 'danger');
	},

	postJSON:           function(url, data, callback) {
		return jQuery.post(url, data, callback, "json");
	}
};

//region Base64
function base64_encode( data ) {	// Encodes data with MIME base64
	//
	// +   original by: Tyler Akins (http://rumkin.com)
	// +   improved by: Bayron Guevara

	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

	do { // pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);

		bits = o1<<16 | o2<<8 | o3;

		h1 = bits>>18 & 0x3f;
		h2 = bits>>12 & 0x3f;
		h3 = bits>>6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		enc += b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	} while (i < data.length);

	switch( data.length % 3 ){
		case 1:
			enc = enc.slice(0, -2) + '==';
			break;
		case 2:
			enc = enc.slice(0, -1) + '=';
			break;
	}

	return enc;
}
function base64_decode( data ) {
	// Decodes data encoded with MIME base64
	//
	// +   original by: Tyler Akins (http://rumkin.com)


	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

	do {  // unpack four hexets into three octets using index points in b64
		h1 = b64.indexOf(data.charAt(i++));
		h2 = b64.indexOf(data.charAt(i++));
		h3 = b64.indexOf(data.charAt(i++));
		h4 = b64.indexOf(data.charAt(i++));

		bits = h1<<18 | h2<<12 | h3<<6 | h4;

		o1 = bits>>16 & 0xff;
		o2 = bits>>8 & 0xff;
		o3 = bits & 0xff;

		if (h3 == 64)	  enc += String.fromCharCode(o1);
		else if (h4 == 64) enc += String.fromCharCode(o1, o2);
		else			   enc += String.fromCharCode(o1, o2, o3);
	} while (i < data.length);

	return enc;
}