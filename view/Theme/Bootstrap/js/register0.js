/**
 * Created by leon0399 on 08.11.2015.
 */
{
	var regFormElem         = $('#registration');
	var usernameGroup       = $('#RegUsername');
	var usernameElem        = regFormElem.find($('input[type=text][name=username]'));
	var genderElem          = regFormElem.find($('select[name=gender]'));
	var emailGroup          = $('#RegEmail');
	var emailElem           = regFormElem.find($('input[name=email]'));
	var emailRepeatGroup    = $('#RegEmailRepeat');
	var emailRepeatElem     = regFormElem.find($('input[name=email-repeat]'));
	var passwordGroup       = $('#RegPassword');
	var passwordElem        = regFormElem.find($('input[name=password]'));
	var passwordRepeatGroup = $('#RegPasswordRepeat');
	var passwordRepeatElem  = regFormElem.find($('input[name=password-repeat]'));
	var execButton          = regFormElem.find($('button#registerButton'));
	var canProceed          = {
		username      : false,
		email         : false,
		emailRepeat   : false,
		password      : false,
		passwordRepeat: false,
		captcha       : false
	};
	var acounter            = 0;
}

$(function() {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	usernameElem.focus();
});

usernameElem.blur(function() {
	checkUsername();
});
emailElem.blur(function() {
	checkEmail();
});
emailRepeatElem.blur(function() {
	checkEmailRepeat();
});
passwordElem.blur(function() {
	checkPassword();
});
passwordRepeatElem.blur(function() {
	checkPasswordRepeat();
});
execButton.click(function() {
	execButton.attr('disabled', 'disabled').addClass('m-progress');

	checkUsername();
	checkEmail();
	checkEmailRepeat();
	checkPassword();
	checkPasswordRepeat();
	checkCaptcha();

	$( document ).ajaxStop(function() {
		if(acounter++ < 1) {
			setTimeout(function () {
				if (canProceed.email
				    && canProceed.emailRepeat
				    && canProceed.password
				    && canProceed.passwordRepeat
				    && canProceed.captcha) {
					$.ajax({
							url    : _defs.url + 'api/users',
							method : 'POST',
							data   : {
								username       : usernameElem.val(),
								email          : emailElem.val(),
								email_repeat   : emailRepeatElem.val(),
								password       : base64_encode(passwordElem.val()),
								password_repeat: base64_encode(passwordRepeatElem.val()),
								gender         : genderElem.val(),
								captcha        : grecaptcha.getResponse()
							},
							success: function (resp) {
								//console.log(resp);
								if($.inArray(resp, 'errors')) {
									console.log(resp.errors);
									$.each(resp.errors, function(elem, index, arr) {
										switch (elem) {
											case 'Username':
												DAMNIT.formGroupDanger(usernameGroup);
												break;
										}
									});
								}
							}
						}
					)
				}
			}, 300);
		}
	});
});


function checkUsername() {
	if (!(_defs.register.username_regex.test(usernameElem.val()))) {
		DAMNIT.formGroupDanger(usernameGroup);
		canProceed.username = false;
	} else {
		$.getJSON(
			_defs.url + 'api/users/' + usernameElem.val(), function (resp) {
				if (resp.status == 404) {
					DAMNIT.formGroupSuccess(usernameGroup);
					canProceed.username = true;
				} else {
					DAMNIT.formGroupDanger(usernameGroup);
					canProceed.username = false;
				}
			}
		).error(function (msg) {
			        DAMNIT.formGroupDanger(usernameGroup);
			        canProceed.username = false;
		        })
	}
}
function checkEmail() {
	if (!(_defs.register.email_regex.test(emailElem.val()))) {
		DAMNIT.formGroupDanger(emailGroup);
		canProceed.email = false;
	} else {
		$.getJSON(
			_defs.url + 'api/users/' + emailElem.val(), function (resp) {
				if (resp.status == 404) {
					DAMNIT.formGroupSuccess(emailGroup);
					canProceed.email = true;
				} else {
					DAMNIT.formGroupDanger(emailGroup);
					canProceed.email = false;
				}
			}
		).error(function (msg) {
			        DAMNIT.formGroupDanger(emailGroup);
			        canProceed.username = false;
		        })
	}
}
function checkEmailRepeat() {
	checkEmail();
	if (!(_defs.register.email_regex.test(emailRepeatElem.val())) || (emailElem.val() != emailRepeatElem.val()) ) {
		DAMNIT.formGroupDanger(emailRepeatGroup);
		canProceed.emailRepeat = false;
	} else {
		//if ((emailElem.val() == emailRepeatElem.val()) && (_defs.register.email_regex.test(emailRepeatElem.val()))) {
			DAMNIT.formGroupSuccess(emailRepeatGroup);
			canProceed.emailRepeat = true;
		/*} else {
			DAMNIT.formGroupDanger(emailRepeatGroup);
			canProceed.emailRepeat = false;
		}*/
	}
}
function checkPassword() {
	if(!_defs.register.password_regex.test(passwordElem.val())) {
		DAMNIT.formGroupDanger(passwordGroup);
		canProceed.password = false;
	} else {
		DAMNIT.formGroupSuccess(passwordGroup);
		canProceed.password = true;
	}
}
function checkPasswordRepeat() {
	checkPassword();
	if(!(_defs.register.password_regex.test(passwordRepeatElem.val())) || (passwordRepeatElem.val() != passwordElem.val())) {
		DAMNIT.formGroupDanger(passwordRepeatGroup);
		canProceed.passwordRepeat = false;
	} else {
		DAMNIT.formGroupSuccess(passwordRepeatGroup);
		canProceed.passwordRepeat = true;
	}
}
function checkCaptcha() {
	canProceed.captcha = grecaptcha.getResponse() != "";
}