{
	var regForm = $("#reg-form");

	var usernameGroup = $("#username-group");
	var usernameInput = usernameGroup.find("#reg-username");

	var emailGroup = $("#email-group");
	var emailInput = emailGroup.find("#reg-email");

	var canRegister = [
		canUsername = false,
	    canEmail = false
	]
}

usernameInput.blur(function() {checkLogin(usernameInput.val())});
function checkLogin(value) {
	var apiUrl = _defs.url + "api/users/" + value;
	console.log(apiUrl);
	if(value == "") {
		DAMNIT.formGroupWarning(usernameGroup);
		canRegister.canUsername = false;
	} else {
		$.getJSON(apiUrl, function(data) {
			console.log(data);
			if(data.status == 404 || data.user == null) {
				DAMNIT.formGroupSuccess(usernameGroup);
				canRegister.canUsername = true;
			} else {
				DAMNIT.formGroupDanger(usernameGroup);
				canRegister.canUsername = false;
			}
		});
	}
}

emailInput.blur(function() {checkEmail(emailInput.val())});
function checkEmail(value) {

}