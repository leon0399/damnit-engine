<?php
/**
 *
 * @author leon0399 <leon.03.99@gmail.com> (http://leon0399.ru/)
 */
{
	$css = array(
			'base',
			'https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css',
			'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css',
			'custom',
	);
	if(!empty($css_files)) {
		$css = array_merge($css, $css_files);
	}
	foreach($css as $file) {
		\DamnIT\Asset\Manager::addCSS($file);
	}
	unset($file);

	$js = array(
			'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
			'https://www.fuelcdn.com/fuelux/3.12.0/js/fuelux.min.js',
			'https://code.jquery.com/ui/1.10.4/jquery-ui.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js',
			'highcharts/highstock',
			'eModal.min',
			'custom',
	);
	if(!empty($js_files)) {
		$js = array_merge($js, $js_files);
	}
	foreach($js as $file) {
		\DamnIT\Asset\Manager::addJS($file);
	}
	unset($file);

	$defer = array();
	if(!empty($defer_files)) {
		$defer = array_merge($defer, $defer_files);
	}
	foreach($defer as $file) {
		\DamnIT\Asset\Manager::addDefer($file);
	}
	unset($file);
}